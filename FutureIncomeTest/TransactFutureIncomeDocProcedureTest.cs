﻿using FutureIncome.Models;
using FutureIncome.Services.Interfaces;
using FutureIncomeTests.Services;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;

namespace FutureIncomeTests
{

    [TestClass]
    public class TransactFutureIncomeDocProcedureTest
    {
        private static WebApplicationFactory<Program> _factory;

        [ClassInitialize]
        public static void ClassInit(TestContext testContext)
        {
            _factory = new WebApplicationFactory<Program>();
        }

        [DataTestMethod]
        [DataRow("fgeye")]
        [DataRow("@525")]
        [DataRow("-151f314")]
        [DataRow("")]
        public void TestTransactDoc_WrongFormat(string id)
        //Проверка на неккоретный формат входного аргумента
        {
            //Arrage
            bool wrongFormat = new();
            //Проверка формата введеного аргумента

            //Act
            wrongFormat = int.TryParse(id, out int result);
            //Пробуем парсить id

            //Assert
            Assert.IsFalse(wrongFormat);
            //Проверяем ,если аргумент не int то неверный формат
        }

        [DataTestMethod]
        [DataRow("536162362")]
        [DataRow("0")]
        [DataRow("-5135141")]
        [DataRow("-35")]

        public void TestDocInsert_IdNotFound(string id)
        //Провека наличия транзакции, который нужно провести
        {
            try
            {
                //Arrage
                int docId = int.Parse(id);
                //Получаем id договора

                var docAccountTransactionService = _factory.Services.GetService<IDocAccountTransactionService>();
                //Получаем сервис
                DocAccountTransaction docAccountTransaction = new();
                //Получем обьект транзакции


                //Act
                docAccountTransaction = docAccountTransactionService.GetByFutureIncomeDocId(docId);
                //Получаем транзакцию


                //Assert
                Assert.IsNull(docAccountTransaction);
                //Проверяем, существует ли документ транзакции
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //Если получили искючение, то тест провален
            }
            finally
            {
                ClearData(id);
                //Очищаем созданные данные
            }

        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void TestDoubleTransact (string doc_id)
        //Проверка попытки провести транзакцию дважды
        {
            try
            {
                //Assert
                int docId = int.Parse(doc_id);
                //парсим ид документа

                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //Создаем сервис

                //Act
                futureIncomeDocService.TransactDoc(docId);
                //Проводим транзакцию в первый раз
                futureIncomeDocService.TransactDoc(docId);
                //Проводим повторное проведение

                //Assert
                Assert.Fail();
            }
            catch (Exception e)
            {
                StringAssert.Contains("Документ уже проведен. Повторное проведение невозможно.", e.Message);
                //Если получили исключение с определенной ошибкой, то тест пройден
            }
            finally
            {
                ClearData(doc_id);
                //Очищаем созданные данные
            }
        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void TestTransactDoc_UpdateDoc(string doc_id)
        {
            try
            {
                //Assert
                int docId = int.Parse(doc_id);
                //парсим ид документа

                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //Создаем сервис
                Doc doc = new();
                

                //Act
                futureIncomeDocService.TransactDoc(docId);
                //Проводим транзакцию
                doc = futureIncomeDocService.GetById(docId);
                //Получаем документ

                //Assert
                Assert.AreEqual(1,doc.State);
                //Проверяем данные
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //Если получили искючение, то тест провален
            }
            finally
            {
                ClearData(doc_id);
                //Очищаем созданные данные
            }
        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void TestTransactDoc_insertAccountTransaction(string doc_id)
        {
            try
            {
                //Assert
                int docId = int.Parse(doc_id);
                //парсим ид документа

                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //Создаем сервис
                Doc doc = new();


                //Act
                futureIncomeDocService.TransactDoc(docId);
                //Проводим транзакцию
                doc = futureIncomeDocService.GetById(docId);
                //Получаем документ

                //Assert
                Assert.AreEqual(1, doc.State);
                //Проверяем данные
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //Если получили искючение, то тест провален
            }
            finally
            {
                ClearData(doc_id);
                //Очищаем созданные данные
            }
        }

        public void ClearData(string doc_id)
        //возвращаем данные в исходное положение
        {
            try
            {
                int id = int.Parse(doc_id);
                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                futureIncomeDocService.CancelTransactDoc(id);
            }
            catch
            {
                return;
            }
        }

        public static IEnumerable<object[]> GetData()
        {
            yield return new object[] { "13879" }; 
            yield return new object[] { "13664" }; 
            yield return new object[] { "13186" }; 
            yield return new object[] { "13285" }; 
            yield return new object[] { "13894" };
            yield return new object[] { "13645" };
            //yield return new object[] { "-1025"};
            //yield return new object[] { "0"};
            //yield return new object[] { "436346362"};
            //yield return new object[] { "ggre"};
            //yield return new object[] { "@$252"};
        }
    }
}
