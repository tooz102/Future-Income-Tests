using FutureIncome.Models;
using FutureIncome.Services;
using FutureIncome.Services.Interfaces;
using FutureIncomeTests.Context;
using FutureIncomeTests.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.VisualStudio.TestPlatform.TestHost;
using System.Data.SqlClient;

namespace FutureIncomeTest
{


    [TestClass]
    public class InsertFutureIncomeDocProcedureTest
    {
        private static WebApplicationFactory<Program> _factory;

        [ClassInitialize]
        public static void ClassInit(TestContext testContext)
        {
            _factory = new WebApplicationFactory<Program>();
        }


        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        //������������ �������� �������� ���������� �� ������� GetData
        public void TestDocInsert(string id)
        //���� ������� ������ � ������� p_future_income_docs
        {
            try
            {
                //Arrage
                //������� ����������
                int agreementId = int.Parse(id);
                //������ �� �������� �� �������� ���������
                var futureIncomeDoc = _factory.Services.GetService<IFutureIncomeDocService>();
                //������� ������ �������.
                Doc doc = new();
                //������� ������ ������ ������ (������� p_future_income_docs)

                //Act
                //���������� ��������
                futureIncomeDoc.Create(agreementId);
                //�������� � ������� ����� Create, �� ������ ������� ������ � ������� p_future_income_docs
                doc = futureIncomeDoc.GetByAgreementId(agreementId);
                //�������� ������ ������ �� ����������� ������

                //Assert
                //���������
                Assert.IsNotNull(doc);
                //���������, ���������� �� ������ ������ (��������� ������ � ��)
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //���� �������� ���������, �� ���� ��������
            }
            finally
            {
                ClearData(id);
                //������� ��������� ������
            }
        }

        [DataTestMethod]
        [DataRow("fgeye")]
        [DataRow("@525")]
        [DataRow("-151f314")]
        [DataRow("")]
        public void TestDocInsert_IdWrongFormat(string id)
        //�������� �� ����������� ������ �������� ���������
        {
            //Arrage
            bool wrongFormat = new();
            //�������� ������� ��������� ���������

            //Act
            wrongFormat = int.TryParse(id,out int result);
            //������� ������� id

            //Assert
            Assert.IsFalse(wrongFormat);
            //��������� ,���� �������� �� int �� �������� ������
        }

        [DataTestMethod]
        [DataRow("536162362")]
        [DataRow("0")]
        [DataRow("-5135141")]
        [DataRow("-35")]

        public void TestDocInsert_IdNotFound(string id)
        //������� ������� ��������, �� ������� ��������� ��������
        {
            try
            {
                //Arrage
                int agreementId = int.Parse(id);
                //�������� id ��������
                
                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //�������� ������
                Doc doc = new();
                //������� ������ ���������


                //Act
                futureIncomeDocService.Create(agreementId);
                //�������� ������� ��������
                doc = futureIncomeDocService.GetByAgreementId(agreementId);
                //�������� ��������� ��������


                //Assert
                Assert.IsNull(doc);
                //���������, ���������� �� ��������
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //���� �������� ���������, �� ���� ��������
            }
            finally
            {
                ClearData(id);
                //������� ��������� ������
            }

        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        //������������ �������� �������� ���������� �� ������� GetData
        public void TestDocValues(string id)
        //���� ������������ ����� ������, ��������� ������������ ������ ����� �������� ���������
        {
            try
            {
                //Arrage
                //������� ����������
                int agreementId = int.Parse(id);
                //������ �� �������� �� �������� ���������
                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //������� ������ �������.
                Doc doc = new();
                //������� ������ ������ ������ (������� p_future_income_docs)

                var agreementService = _factory.Services.GetService<IAgreementService>();
                //������� ������ �������.
                Agreement agreement = new();
                //������� ������ ������ ������ (������� p_agreements)

                var groupService = _factory.Services.GetService<IGroupService>();
                //������� ������ �������.
                Group group = new Group();
                //������� ������ ������ ������ (������� groups)

                var specialityService = _factory.Services.GetService<ISpecialityService>();
                //������� ������ �������.
                Speciality specality = new();
                //������� ������ ������ ������ (������� specialities)

                var facultyService = _factory.Services.GetService<IFacultyService>();
                //������� ������ �������.
                Faculty faculty = new();
                //������� ������ ������ ������ (������� faculties)



                //Act
                //���������� ��������
                futureIncomeDocService.Create(agreementId);
                //�������� � ������� ����� Create, �� ������ ������� ������ � ������� p_future_income_docs
                doc = futureIncomeDocService.GetByAgreementId(agreementId);
                agreement = agreementService.GetById(agreementId);
                group = groupService.GetById(agreement.Group_id);
                specality = specialityService.GetById(group.Speciality_id);
                faculty = facultyService.GetById(specality.Faculty_id);
                //�������� ������� ������� �� ����������� �����

                if (agreement.Notes is null)
                //���� � ���� Note ������� p_agreements ����� "" �� � ������ ������ ������ ������� �������� null
                {
                    agreement.Notes = "";
                    //���������� ������� �� ""
                }


                //Assert
                //���������
                Assert.AreEqual<int>(1, doc.Doc_Kind_Id);
                //��������� ��� ���������
                Assert.AreEqual<string>(agreement.Doc_num, doc.Doc_Num);
                //��������� ����� ���������
                Assert.AreEqual<bool>(true, doc.State);
                //��������� ��������� ���������
                Assert.AreEqual<bool>(false, doc.Canceled);
                //��������� �������� ���������
                Assert.AreEqual<string>(agreement.Notes, doc.Comment);
                //��������� ����������� � ���������
                Assert.AreEqual<string>("dbo", doc.Create_User);
                //��������� ���������
                Assert.AreEqual<string>("dbo", doc.Edit_User);
                //��������� ���������
                Assert.AreEqual<int>(agreement.Education_form_id, doc.Education_Form_Id);
                //��������� ����� ��������
                Assert.AreEqual<int>(faculty.Id, doc.Faculty_id);
                //��������� ���������
                Assert.AreEqual<int>(faculty.Affiliate_Id, doc.Affiliate_id);
                //��������� ������
                Assert.AreEqual<DateTime>(agreement.Begin_date, doc.Date_From);
                //��������� ���� ������
                Assert.AreEqual<DateTime>(agreement.End_date, doc.Date_To);
                //��������� ���� ���������
                Assert.AreEqual<int>(agreementId, doc.Agreement_Id);
                //��������� �������� � ��������
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //���� �������� ���������, �� ���� ��������
            }
            finally
            {
                ClearData(id);
                //������� ��������� ������
            }
        }


        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void TestDocTable_Insert(string id)
        //����, ���������� �� ������ � ������� p_future_income_doc_table
        {
            try
            {
                //Arrage
                //������� ����������
                int agreementId = int.Parse(id);
                //������ �� �������� �� �������� ���������
                var futureIncomeDocTableService = _factory.Services.GetService<IFutureIncomeDocTableService>();
                //������� ������ �������.
                DocTable docTable = new();

                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //������� ������ �������.
                Doc doc = new();
                //�������� ������� ������� �� ����������� �����


                //Act
                //���������� ��������
                futureIncomeDocService.Create(agreementId);
                //������� ��������
                doc = futureIncomeDocService.GetByAgreementId(agreementId);
                docTable = futureIncomeDocTableService.GetByDocId(doc.Id);
                //�������� ������� �������
               

                //Assert
                //���������
                Assert.IsNotNull(docTable);
                //���������, ���� �� ������ � �������.
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //���� ���������� �� ���� ��������
            }
            finally
            {
                ClearData(id);
                //������� ������
            }
        }



        [DataTestMethod]
        [DataRow("66162", "129945")] // "129944.75")]
        [DataRow("68377", "69231")]// "69230.722")]
        [DataRow("68468", "0")]// "0")]
        [DataRow("68486", "89945")]// "89944.7505")]

        public void TestDocTable_Values(string id,string Summ)
        //�������� �������� ����������� ������ � ������� p_future_income_doc_table
        {
            try
            {
                //Arrage
                //������� ����������
                int agreementId = int.Parse(id);
                int expSumm = int.Parse(Summ);
                //������ �� �������� �� �������� ���������
                var futureIncomeDocTableService = _factory.Services.GetService<IFutureIncomeDocTableService>();
                //������� ������ �������.
                DocTable docTable = new();

                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //������� ������ �������.
                Doc doc = new();

                var personAccountService = _factory.Services.GetService<IPersonAccountService>();
                //������� ������ �������.
                PersonAccount personAccount = new();

                var agreementService = _factory.Services.GetService<IAgreementService>();
                //������� ������ �������.
                Agreement agreement = new();



                //Act
                //���������� ��������
                futureIncomeDocService.Create(agreementId);
                doc = futureIncomeDocService.GetByAgreementId(agreementId);
                docTable = futureIncomeDocTableService.GetByDocId(doc.Id);
                agreement = agreementService.GetById(agreementId);
                personAccount = personAccountService.GetByAgeementId(agreement.Main_Agreement_id);
                //�������� ������� �������

                if (docTable.Comment is null)
                {
                    docTable.Comment = "";
                }
                //����������� null � ������ ���� string


                //Assert
                //���������
                Assert.AreEqual(doc.Id, docTable.Doc_Id);
                Assert.AreEqual(personAccount.Id, docTable.Person_Account_Id);
                Assert.AreEqual(expSumm,docTable.Summ);
                Assert.AreEqual(doc.Comment, docTable.Comment);
                Assert.AreEqual(false, docTable.Is_Hand_Corrected);
                //��������� �������� � �������
             }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //���� ���������� �� ���� ��������
            }
            finally
            {
                ClearData(id);
                //������� ������
            }
        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void TestDocTransaction_Insert(string id)
        //�������� ������� ������ � ������� p_future_income_account_transactions
        {
            try
            {
                //Arrage
                //������� ����������
                int agreementId = int.Parse(id);
                //������ �� �������� �� �������� ���������
                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //������� ������ �������.
                Doc doc = new();

                var docAccountTransactionService = _factory.Services.GetService<IDocAccountTransactionService>();
                //������� ������ �������
                DocAccountTransaction accountTransaction = new();

                //Act
                //���������� ��������
                futureIncomeDocService.Create(agreementId);
                doc = futureIncomeDocService.GetByAgreementId(agreementId);
                accountTransaction = docAccountTransactionService.GetByFutureIncomeDocId(doc.Id);
                //�������� ������� �������

                //Assert
                //���������
                Assert.IsNotNull(accountTransaction);
                //���������, ���������� �� ������
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //���� ���������� �� ���� ��������
            }
            finally
            {
                ClearData(id);
                //������� ������
            }
        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void TestDocTransaction_Values(string id)
        //���� �������� ����������� ������ ������� p_future_income_account_transactions
        {
            try
            {
                //Arrage
                //������� ����������
                int agreementId = int.Parse(id);
                //������ �� �������� �� �������� ���������
                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                Doc doc = new();

                var futureIncomeDocTableService = _factory.Services.GetService<IFutureIncomeDocTableService>();
                DocTable docTable = new();

                var DocKindService = _factory.Services.GetService<IDocKindService>();
                DocKind docKind = new();

                var docAccountTransactionService = _factory.Services.GetService<IDocAccountTransactionService>();
                DocAccountTransaction accountTransaction = new();
                //������� ������� ��������

                //Act
                //���������� ��������
                futureIncomeDocService.Create(agreementId);
                doc = futureIncomeDocService.GetByAgreementId(agreementId);
                accountTransaction = docAccountTransactionService.GetByFutureIncomeDocId(doc.Id);
                docTable = futureIncomeDocTableService.GetByDocId(doc.Id);
                docKind = DocKindService.GetById(doc.Doc_Kind_Id);
                //�������� ������� ������

                //Assert
                //���������
                Assert.AreEqual(doc.Date,accountTransaction.Date);
                Assert.AreEqual(docTable.Summ, accountTransaction.Summ);
                Assert.AreEqual(docKind.Pay_Direction_Id,accountTransaction.Transaction_Kind);
                Assert.AreEqual(docTable.Person_Account_Id,accountTransaction.Person_Account_Id);
                Assert.AreEqual(doc.Id,accountTransaction.Future_Income_Doc_Id);
                Assert.AreEqual(docKind.Operation_Type_Id,accountTransaction.Operation_Type_Id);
                //��������� ������ �� ����������� ������
            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //���� ���������� �� ���� ��������
            }
            finally
            {
                ClearData(id);
                //������� ������
            }
        }

        [DataTestMethod]
        [DynamicData(nameof(GetData), DynamicDataSourceType.Method)]
        public void TestTransactionsTotal_Insert(string id)
        //�������� ������� ������ � ������� p_future_income_transaction_totals
        {
            try
            {
                //Arrage
                //������� ����������
                int agreementId = int.Parse(id);
                //������ �� �������� �� �������� ���������
                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                Doc doc = new();

                var transcationsTotalService = _factory.Services.GetService<ITransactionsTotalService>();
                TransactionsTotal transcationsTotal = new();

                var docAccountTransactionService = _factory.Services.GetService<IDocAccountTransactionService>();
                DocAccountTransaction docAccountTransaction = new();
                //������� ������ ��������.

                //Act
                futureIncomeDocService.Create(agreementId);
                doc = futureIncomeDocService.GetByAgreementId(agreementId);
                docAccountTransaction = docAccountTransactionService.GetByFutureIncomeDocId(doc.Id);
                transcationsTotal = transcationsTotalService.GetByAccountId(docAccountTransaction.Id);
                //�������� ������� ������

                //Assert
                Assert.IsNotNull(transcationsTotal);
                //��������� ������� ����������� ������

            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
                //���� ���������� �� ���� ��������
            }
            finally
            {
                ClearData(id);
                //������� ������
            }
        }

        [DataTestMethod]
        [DataRow("66162", "129945")] // "129944.75")]
        [DataRow("68377", "69231")]// "69230.722")]
        [DataRow("68468", "0")]// "0")]
        [DataRow("68486", "89945")]// "89944.7505")]
        public void TestTransactionsTotal_Values(string id,string summ)
        {
            try
            {
                //Arrage
                //������� ����������
                int agreementId = int.Parse(id);
                int expSum = int.Parse(summ);
                //������ �� �������� �� �������� ���������
                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                //������� ������ �������.
                Doc doc = new();

                var transcationsTotalService = _factory.Services.GetService<ITransactionsTotalService>();
                TransactionsTotal transcationsTotal = new();

                var docAccountTransactionService = _factory.Services.GetService<IDocAccountTransactionService>();
                DocAccountTransaction docAccountTransaction = new();

                //Act
                futureIncomeDocService.Create(agreementId);
                doc = futureIncomeDocService.GetByAgreementId(agreementId);
                docAccountTransaction = docAccountTransactionService.GetByFutureIncomeDocId(doc.Id);
                transcationsTotal = transcationsTotalService.GetByAccountId(docAccountTransaction.Id);

                //Assert
                Assert.AreEqual(expSum, transcationsTotal.Total_Summ);

            }
            catch (Exception e)
            {
                Assert.Fail($@"Test failed: {e.Message} ");
            }
            finally
            {
                ClearData(id);
            }
        }

        public void ClearData(string id)
        {
            try
            {
                int agreementId = int.Parse(id);
                //������ �� �������� �� �������� ���������

                var futureIncomeDocService = _factory.Services.GetService<IFutureIncomeDocService>();
                int docId = futureIncomeDocService.GetByAgreementId(agreementId).Id;
                //������� ������ �������.

                CancelTransaction(docId);
                //�������� ����������
                futureIncomeDocService!.DeleteByAgreementId(agreementId);
                //������� ������ � �� �������
            }
            catch
            {
                return;
            }
        }

        public void CancelTransaction(int docId)
        {
            var futureIncomeDoc = _factory.Services.GetService<IFutureIncomeDocService>();
            //������� ������ �������.
            try
            {
                futureIncomeDoc.CancelTransactDoc(docId);
            }
            catch 
            {
                return;
            }
        }

        public static IEnumerable<object[]> GetData()
        {
            yield return new object[] { "66162" }; //����� 1299445
            yield return new object[] { "68468" }; //����� 209231
            yield return new object[] { "68377" }; //����� 69231
            yield return new object[] { "68486" }; //����� 89945
            yield return new object[] { "72374" };
            yield return new object[] { "71180" };
            //yield return new object[] { "-1025"};
            //yield return new object[] { "0"};
            //yield return new object[] { "436346362"};
            //yield return new object[] { "ggre"};
            //yield return new object[] { "@$252"};
        }
    }
}

