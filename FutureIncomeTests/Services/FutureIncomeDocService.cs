﻿using Dapper;
using FutureIncome.Models;
using FutureIncomeTests.Context;
using System.Data.SqlClient;

namespace FutureIncomeTests.Services
{
    public class FutureIncomeDocService : IFutureIncomeDocService
    {
        private readonly ApplicationContext _context;

        public FutureIncomeDocService(ApplicationContext context)
        {
            _context = context;
        }

        public void Create(int docId)
        {
            using (var connection = _context.CreateConnection())
            {
                connection.Query($"exec p_insert_future_income {docId}");
            }

        }

        public List<Doc> GetAll()
        {
            using (var connection = _context.CreateConnection())
            {
                var docs = connection.Query<Doc>($@"SELECT * FROM p_future_income_docs");
                List<Doc> result = new List<Doc>();
                foreach(var doc in docs)
                {
                    result.Add(doc);
                }
                return result;
            }
        }

        public Doc GetById(int docId)
        {
            using (var connection = _context.CreateConnection())
            {
                Doc result = connection.QuerySingle<Doc>($@"SELECT * FROM p_future_income_docs WHERE id = {docId}");
                return result;
            }
        }

        public Doc GetByAgreementId(int agreementId)
        {
            using (var connection = _context.CreateConnection())
            {
                Doc result = connection.Query<Doc>($@"SELECT * FROM p_future_income_docs WHERE agreement_id = {agreementId}").FirstOrDefault();
                return result;
            }
        }

        public void DeleteById (int docId)
        {
            using (var connection = _context.CreateConnection())
            {
                connection.Query($@"DELETE FROM p_future_income_docs WHERE id = {docId}");
            }
        }

        public void DeleteByAgreementId (int agreementId)
        {
            var sql = $"SELECT TOP(1) id FROM p_future_income_docs WHERE agreement_id = {agreementId} ORDER BY ID DESC";
            using (var connection = _context.CreateConnection())
            {
                try
                {
                    var docId = connection.QuerySingle<int>(sql);
                    connection.Query($@"DELETE FROM p_future_income_docs WHERE id = {docId}");
                }
                catch
                {
                    return;
                }
            }

        }

        public void TransactDoc(int docId)
        {
            using (var connection = _context.CreateConnection())
            {
                connection.Query($@"exec [p_transact_future_income_doc] {docId}");
            }

        }

        public void CancelTransactDoc(int docId)
        {
            using (var connection = _context.CreateConnection())
            {
                connection.Query($@"DELETE FROM p_future_income_account_transactions WHERE future_income_doc_id = {docId}");
                connection.Query($@"UPDATE p_future_income_docs
		                                    SET state = 0
		                                    WHERE id = {docId}");
            }
        }
    }
}