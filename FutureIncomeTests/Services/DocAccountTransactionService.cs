﻿using Dapper;
using FutureIncome.Models;
using FutureIncome.Services.Interfaces;
using FutureIncomeTests.Context;
using System.Data.SqlClient;

namespace FutureIncome.Services
{
    public class DocAccountTransactionService : IDocAccountTransactionService
    {
        private readonly ApplicationContext _context;

        public DocAccountTransactionService(ApplicationContext context)
        {
            _context = context;
        }

        public DocAccountTransaction GetById (int id)
        {
            using (var connection = _context.CreateConnection())
            {
                DocAccountTransaction result = connection.QuerySingle<DocAccountTransaction>($@"select * from p_future_income_account_transactions where id = {id}");
                return result;
            }
        }

        public DocAccountTransaction GetByDocId(int docId)
        {
            using (var connection = _context.CreateConnection())
            {
                DocAccountTransaction result = connection.QuerySingle<DocAccountTransaction>($@"select * from p_future_income_account_transactions where future_income_doc_id = {docId}");
                return result;
            }
        }

        public DocAccountTransaction GetByFutureIncomeDocId(int docId)
        {
            using (var connection = _context.CreateConnection())
            {
                DocAccountTransaction result = connection.Query<DocAccountTransaction>($@"select * from p_future_income_account_transactions where future_income_doc_id = {docId}").FirstOrDefault();
                return result;
            }
        }

        public void TransactDoc (int id)
        {
            using (var connection = _context.CreateConnection())
            {
                connection.Query($@"exec p_transact_future_income_doc {id}");
            }
        }
    }
}
