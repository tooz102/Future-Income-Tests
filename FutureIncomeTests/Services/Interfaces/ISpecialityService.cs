﻿using FutureIncome.Models;

namespace FutureIncome.Services
{
    public interface ISpecialityService
    {
        public Speciality GetById(int specialityId);
    }
}
