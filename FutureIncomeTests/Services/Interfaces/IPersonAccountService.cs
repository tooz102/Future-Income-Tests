﻿using FutureIncome.Models;

namespace FutureIncome.Services
{
    public interface IPersonAccountService
    {
        public PersonAccount GetByAgeementId(int agreementId);
    }
}
