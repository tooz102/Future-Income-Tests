﻿using FutureIncome.Models;

namespace FutureIncome.Services
{
    public interface IFutureIncomeDocTableService
    {
        public DocTable GetByDocId(int docId);
    }
}
