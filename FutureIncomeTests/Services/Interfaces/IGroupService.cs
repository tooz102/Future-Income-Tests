﻿using FutureIncome.Models;

namespace FutureIncome.Services
{
    public interface IGroupService
    {
        public Group GetById(int groupId);
    }
}
