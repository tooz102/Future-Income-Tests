﻿using FutureIncome.Models;

namespace FutureIncome.Services
{
    public interface IFacultyService
    {
        public Faculty GetById(int facultyId);
    }
}
