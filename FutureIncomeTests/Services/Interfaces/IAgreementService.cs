﻿using FutureIncome.Models;

namespace FutureIncome.Services
{
    public interface IAgreementService
    {
        public List<Agreement> GetAll();
        public Agreement GetById(int id);
    }
}
