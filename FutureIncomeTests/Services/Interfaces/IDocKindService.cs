﻿using FutureIncome.Models;

namespace FutureIncome.Services.Interfaces
{
    public interface IDocKindService
    {
        public DocKind GetById(int id);
    }
}
