﻿using FutureIncome.Models;

namespace FutureIncomeTests.Services
{
    public interface IFutureIncomeDocService
    {
        void Create(int docId);

        public List<Doc> GetAll();
        public Doc GetById(int docId);
        public Doc GetByAgreementId(int agreementId);

        public void DeleteById(int docId);
        public void DeleteByAgreementId(int agreementId);

        public void TransactDoc(int docId);
        public void CancelTransactDoc(int docId);

    }
}
