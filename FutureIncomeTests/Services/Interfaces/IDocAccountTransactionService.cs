﻿using FutureIncome.Models;

namespace FutureIncome.Services.Interfaces
{
    public interface IDocAccountTransactionService
    {
        public DocAccountTransaction GetByFutureIncomeDocId(int docId);
        public DocAccountTransaction GetById(int Id);

    }
}
