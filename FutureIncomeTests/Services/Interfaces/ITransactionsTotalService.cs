﻿using FutureIncome.Models;

namespace FutureIncome.Services.Interfaces
{
    public interface ITransactionsTotalService
    {
        public TransactionsTotal GetByAccountId(int accountId);
    }
}
