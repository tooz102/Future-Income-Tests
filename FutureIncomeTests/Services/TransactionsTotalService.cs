﻿using Dapper;
using FutureIncome.Models;
using FutureIncome.Services.Interfaces;
using FutureIncomeTests.Context;

namespace FutureIncome.Services
{
    public class TransactionsTotalService : ITransactionsTotalService
    {
        private readonly ApplicationContext _context;

        public TransactionsTotalService(ApplicationContext context)
        {
            _context = context;
        }

        public TransactionsTotal GetByAccountId (int accountId)
        {
            using(var connection = _context.CreateConnection())
            {
                TransactionsTotal result = connection.QuerySingle<TransactionsTotal>($@"select * from p_future_income_transaction_totals where account_transaction_id = {accountId}");
                return result;
            }
        }
    }
}
