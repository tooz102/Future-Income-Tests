﻿using Dapper;
using FutureIncome.Models;
using FutureIncomeTests.Context;
using System.Data.SqlClient;

namespace FutureIncome.Services
{
    public class AgreementService: IAgreementService
    {
        private readonly ApplicationContext _context;

        public AgreementService(ApplicationContext context)
        {
            _context = context;
        }


        public List<Agreement> GetAll()
        {
            using (var connection = _context.CreateConnection()) 
            {
                return connection.Query<Agreement>($@"SELECT * FROM p_agreements").ToList();
            }
        }

        public Agreement GetById(int agreementId)
        {
            using (var connection = _context.CreateConnection()) 
            {
                return connection.QuerySingle<Agreement>($@"SELECT * FROM p_agreements WHERE id = {agreementId}");
            }
        }
    }
}
