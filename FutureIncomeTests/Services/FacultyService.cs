﻿using Dapper;
using FutureIncome.Models;
using FutureIncomeTests.Context;
using System.Data.SqlClient;

namespace FutureIncome.Services
{
    public class FacultyService :IFacultyService
    {
        private readonly ApplicationContext _context;

        public FacultyService(ApplicationContext context)
        {
            _context = context;
        }

        public Faculty GetById(int facultyId)
        {
            using (var connection = _context.CreateConnection())
            {
                Faculty result = connection.QuerySingle<Faculty>($@"SELECT * FROM faculties WHERE id = {facultyId}");
                return result;
            }
        }
    }
}
