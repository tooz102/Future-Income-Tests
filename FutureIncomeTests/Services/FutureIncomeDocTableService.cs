﻿using Dapper;
using FutureIncome.Models;
using FutureIncomeTests.Context;
using System.Data.SqlClient;

namespace FutureIncome.Services
{
    public class FutureIncomeDocTableService : IFutureIncomeDocTableService
    {
        private readonly ApplicationContext _context;
        public FutureIncomeDocTableService(ApplicationContext context)
        {
            _context = context;
        }

        public DocTable GetByDocId(int docId)
        {
            using (var connection = _context.CreateConnection())
            {
                DocTable result = connection.QuerySingle<DocTable>($@"SELECT * FROM p_future_income_doc_table WHERE doc_id = {docId}");
                return result;
            }
        }
    }
}
