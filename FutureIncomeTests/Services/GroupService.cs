﻿using Dapper;
using FutureIncome.Models;
using FutureIncomeTests.Context;
using System.Data.SqlClient;

namespace FutureIncome.Services
{
    public class GroupService : IGroupService
    {
        private readonly ApplicationContext _context;

        public GroupService(ApplicationContext context)
        {
            _context = context;
        }

        public Group GetById (int groupId)
        {
            using (var connection = _context.CreateConnection())
            {
                Group result = connection.QuerySingle<Group>($@"SELECT * FROM groups WHERE id = {groupId}");
                return result;
            }
        }
    }
}
