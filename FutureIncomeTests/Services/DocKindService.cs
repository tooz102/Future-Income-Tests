﻿using Dapper;
using FutureIncome.Models;
using FutureIncome.Services.Interfaces;
using FutureIncomeTests.Context;

namespace FutureIncome.Services
{
    public class DocKindService : IDocKindService
    {
        private readonly ApplicationContext _context;

        public DocKindService(ApplicationContext context)
        {
            _context = context;
        }

        public DocKind GetById (int id)
        {
            using (var connection = _context.CreateConnection())
            {
                DocKind result = connection.QuerySingle<DocKind>($@"SELECT * FROM p_future_income_doc_kinds WHERE id = {id}");
                return result;
            }
        }

    }
}
