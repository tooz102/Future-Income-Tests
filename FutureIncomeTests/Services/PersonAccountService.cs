﻿using Dapper;
using FutureIncome.Models;
using FutureIncomeTests.Context;
using System.Data.SqlClient;

namespace FutureIncome.Services
{
    public class PersonAccountService: IPersonAccountService
    {
        private readonly ApplicationContext _context;

        public PersonAccountService(ApplicationContext context)
        {
            _context = context;
        }

        public PersonAccount GetByAgeementId(int agreementId)
        {
            using (var connection = _context.CreateConnection())
            {
                PersonAccount result = connection.QuerySingle<PersonAccount>($@"SELECT * FROM p_person_accounts WHERE main_agreement_id = {agreementId}");
                return result;
            }

        }
    }
}
