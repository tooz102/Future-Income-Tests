﻿using Dapper;
using FutureIncome.Models;
using FutureIncomeTests.Context;
using System.Data.SqlClient;

namespace FutureIncome.Services
{
    public class SpecialityService: ISpecialityService
    {
        private readonly ApplicationContext _context;

        public SpecialityService(ApplicationContext context)
        {
            _context = context;
        }

        public Speciality GetById(int specialityId)
        {
            using (var connection = _context.CreateConnection())
            {
                Speciality result = connection.QuerySingle<Speciality>($@"SELECT * FROM specialities WHERE id = {specialityId}");
                return result;
            }
        }
    }
}
