﻿namespace FutureIncome.Models
{
    public class DocKind
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int? Operation_Type_Id { get; set; }
        public int? Pay_Direction_Id { get; set; }
    }
}
