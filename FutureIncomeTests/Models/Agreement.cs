﻿namespace FutureIncome.Models
{
    public class Agreement
    {
        public int Doc_kind_id { get; set; }
        public string? Doc_num { get; set; }
        public string? Notes { get; set; }
        public string? Create_user { get; set; }
        public string? Edit_user { get; set; }
        public int Education_form_id { get; set; }
        public DateTime Begin_date { get; set; }
        public DateTime End_date { get; set; }
        public int Group_id { get; set; }
        public int Main_Agreement_id { get; set; }

    }
}
