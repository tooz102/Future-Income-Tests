﻿namespace FutureIncome.Models
{
    public class DocAccountTransaction
    {
        public int Id { get; set; }
        public DateTime Date { get; set; }
        public int Person_Account_Id { get; set; }
        public float Summ { get; set; }
        public int Transaction_Kind { get; set; }
        public int Operation_Type_Id { get; set; }
        public int Future_Income_Doc_Id { get; set; }
        public DateTime Create_Date { get; set; }
        public string? Create_User { get; set; }
        public DateTime Edit_Date { get; set; } 
        public string? Edit_User { get; set; }
    }
}
