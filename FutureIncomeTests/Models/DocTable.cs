﻿namespace FutureIncome.Models
{
    public class DocTable
    {
        public int Id { get; set; }
        public int Doc_Id { get; set; }
        public int Person_Account_Id { get; set; }
        public int Summ { get; set; }
        public string? Comment { get; set; }
        public Boolean Is_Hand_Corrected { get; set; }
                 
    }
}
