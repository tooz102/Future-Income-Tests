﻿namespace FutureIncome.Models
{
    public class Doc
    {
        public int Id { get; set; }
        public int Doc_Kind_Id { get; set; }
        public DateTime Date { get; set; }
        public string? Doc_Num { get; set; }
        public bool State { get; set; }
        public bool Canceled { get; set; }
        public string? Comment { get; set; }
        public int Order_Id_Base { get; set; }
        public int Affiliate_id { get; set; }
        public int Faculty_id { get; set; }
        public int Education_Form_Id { get; set; }
        public DateTime Date_From { get; set; }
        public DateTime Date_To { get; set; }
        public DateTime Create_Date { get; set; }
        public string? Create_User { get; set; }
        public DateTime Edit_Date { get; set; }
        public string? Edit_User { get; set; }
        public int Agreement_Id { get; set; }
    }
}
