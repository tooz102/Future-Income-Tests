﻿namespace FutureIncome.Models
{
    public class Faculty
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public int Affiliate_Id { get; set; }
    }
}
