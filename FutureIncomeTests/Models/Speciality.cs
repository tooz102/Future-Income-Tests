﻿namespace FutureIncome.Models
{
    public class Speciality
    {
        public int Id { get; set; }
        public string? Name { get; set; }    
        public int Faculty_id { get; set; }
    }
}
