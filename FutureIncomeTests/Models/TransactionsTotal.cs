﻿namespace FutureIncome.Models
{
    public class TransactionsTotal
    {
        public int Account_Transaction_Id { get; set; }
        public int Total_Summ { get; set; }
    }
}
