using FutureIncome.Services;
using FutureIncome.Services.Interfaces;
using FutureIncomeTests.Context;
using FutureIncomeTests.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSingleton<ApplicationContext>();
builder.Services.AddTransient<IAgreementService,AgreementService>();
builder.Services.AddTransient<IDocAccountTransactionService, DocAccountTransactionService>();
builder.Services.AddTransient<IFutureIncomeDocService, FutureIncomeDocService>();
builder.Services.AddTransient<IFacultyService, FacultyService>();
builder.Services.AddTransient<IFutureIncomeDocTableService, FutureIncomeDocTableService>();
builder.Services.AddTransient<IGroupService, GroupService>();
builder.Services.AddTransient<IPersonAccountService, PersonAccountService>();
builder.Services.AddTransient<ISpecialityService, SpecialityService>();
builder.Services.AddTransient<IDocKindService, DocKindService>();
builder.Services.AddTransient<ITransactionsTotalService, TransactionsTotalService>();


var app = builder.Build();

app.MapGet("/", () => "Hello World!");

app.Run();

public partial class Program { }