﻿using Dapper;
using System.Data;

namespace FutureIncomeTests
{
    public interface IDapperWrapper
    {
        IEnumerable<T> Query<T>(IDbConnection connection, string sql);
    }
    public class DapperWrapper : IDapperWrapper
    {
        public IEnumerable<T> Query<T>(IDbConnection connection, string sql)
        {
            return connection.Query<T>(sql);
        }
    }
}
